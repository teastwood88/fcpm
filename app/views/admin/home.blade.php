@extends('adminMaster')

@section('content')
	<h2>Manage Properties - <a href="{{URL::to('admin/property/add')}}" class="btn btn-primary">Add Property</a></h2>

	<table class="table">
		<thead>
			<tr>
				<th>City</th>
				<th>Address</th>
				<th>Bedrooms</th>
				<th>Bathrooms</th>
				<th>Cost</th>
				<th>Is Rented</th>
				<th>Is Featured</th>		
				<th></th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($properties as $p)
				<tr>
					<th>{{$p->city}}</th>
					<th>{{$p->address}}</th>
					<th>{{$p->bedrooms}}</th>
					<th>{{$p->bathrooms}}</th>
					<th>{{$p->cost}}</th>	
					<th>@if($p->is_rented == 1)<span class="glyphicon glyphicon-ok"></span>@endif</th>
					<th>@if($p->is_featured == 1)<span class="glyphicon glyphicon-ok"></span>@endif</th>	
					<th><a href="{{URL::to('admin/property/photos/'.$p->id)}}">Photos ({{count($p->photos )}})</a></th>
					<th><a href="{{URL::to('admin/property/edit/'.$p->id)}}">Edit</a></th>
					<th><a href="{{URL::to('admin/property/delete/'.$p->id)}}" onclick="javascript:return confirm('Are you sure you want to delete this entry?')">Delete</a></th>
				</tr>
			@endforeach
		</tbody>
	</table>

@stop