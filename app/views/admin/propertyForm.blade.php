@extends('adminMaster')

@section('content')
	@if(isset($property))
		<h2>Edit Property</h2>
	@else
		<h2>Add Property</h2>
	@endif

	@if(isset($property))
		<form role="form" method="post" action="{{ action('AdminController@edit_property_post') }}">
	@else
		<form role="form" method="post" action="{{ action('AdminController@add_property_post') }}">
	@endif
		<div class="form-group">
			<label for="city">City</label>
			<input type="text" class="form-control" name="city" placeholder="London" value="@if(isset($property)){{$property->city}}@endif">
		</div>
		<div class="form-group">
			<label for="address">Address</label>
			<input type="address" class="form-control" name="address" placeholder="67 Blackburn Cres" value="@if(isset($property)){{$property->address}}@endif">
		</div>
		<div class="form-group">
			<label for="unit_number">Unit Number</label>
			<input type="text" class="form-control" name="unit_number" placeholder="12" value="@if(isset($property)){{$property->unit_number}}@endif">
		</div>
		<div class="form-group">
			<label for="postal_code">Postal Code</label>
			<input type="text" class="form-control" name="postal_code" placeholder="N0L 1R0" value="@if(isset($property)){{$property->postal_code}}@endif">
		</div>
		<div class="form-group">
			<label for="cost">Cost (monthly)</label>
			<input type="text" class="form-control" name="cost" placeholder="750" value="@if(isset($property)){{$property->cost}}@endif">
		</div>

		<label for="bedrooms">Bedrooms</label>
		<select class="form-control" name="bedrooms">
			<option @if(isset($property) && $property->bedrooms == 1) selected @endif>1</option>
			<option @if(isset($property) && $property->bedrooms == 2) selected @endif>2</option>
			<option @if(isset($property) && $property->bedrooms == 3) selected @endif>3</option>
			<option @if(isset($property) && $property->bedrooms == 4) selected @endif>4</option>
			<option @if(isset($property) && $property->bedrooms == 5) selected @endif>5</option>
			<option @if(isset($property) && $property->bedrooms == 6) selected @endif>6</option>
			<option @if(isset($property) && $property->bedrooms == 7) selected @endif>7</option>
		</select>

		<label for="bathrooms">Bathrooms</label>
		<select class="form-control" name="bathrooms">
			<option @if(isset($property) && $property->bathrooms == 1) selected @endif>1</option>
			<option @if(isset($property) && $property->bathrooms == 1.5) selected @endif>1.5</option>
			<option @if(isset($property) && $property->bathrooms == 2) selected @endif>2</option>
			<option @if(isset($property) && $property->bathrooms == 2.5) selected @endif>2.5</option>
			<option @if(isset($property) && $property->bathrooms == 3) selected @endif>3</option>
			<option @if(isset($property) && $property->bathrooms == 3.5) selected @endif>3.5</option>
			<option @if(isset($property) && $property->bathrooms == 4) selected @endif>4</option>
			<option @if(isset($property) && $property->bathrooms == 4.5) selected @endif>4.5</option>
			<option @if(isset($property) && $property->bathrooms == 5) selected @endif>5</option>
			<option @if(isset($property) && $property->bathrooms == 5.5) selected @endif>5.5</option>
		</select>

		<div class="checkbox">
		  <label>
		    <input type="checkbox" value="1" name="is_rented" @if(isset($property) && $property->is_rented == 1) checked @endif>
		    Check if property is rented.
		  </label>
		</div>

		<div class="checkbox">
		  <label>
		    <input type="checkbox" value="1" name="is_featured" @if(isset($property) && $property->is_featured == 1) checked @endif>
		    Check if property is featured.
		  </label>
		</div>

		<label>Extras</label>
		<div class="checkbox">
			@foreach($extras as $e)
				<label>
				    <input type="checkbox" value="1" name="extras[{{$e}}]" @if(isset($property) &&is_array(json_decode($property->extras, true)) ) @if( $property->extras != NULL && array_key_exists($e, json_decode($property->extras, true) ) ) checked @endif @endif> {{$e}}
				</label>
			@endforeach
		</div>

		<div class="form-group">
			<label for="description">Description</label>
			<textarea class="form-control" rows="3" name="description">@if(isset($property)){{$property->description}}@endif</textarea>
		</div>

		@if(isset($property))
			<input type="hidden" name="property_id" value="{{$property->id}}">
		@endif

		<button type="submit" class="btn btn-default">Submit</button>
	</form>


@stop