@extends('adminMaster')

@section('content')
	<h2>Contacts</h2>
	<table class="table">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Message</th>
			</tr>
		</thead>
		<tbody>
			@foreach($contacts as $c)
				<tr>
					<th>{{$c->name}}</th>
					<th><a href="mailto:{{$c->email}}">{{$c->email}}</a></th>
					<th>{{$c->message}}</th>
				</tr>
			@endforeach
		</tbody>
	</table>

@stop