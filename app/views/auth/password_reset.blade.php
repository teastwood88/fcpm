@extends('adminMaster')

@section('content')
	
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h1>Reset Password</h1>
			<form role="form" method="POST" action="{{ action('AuthController@password_reset_post') }}">
				<div class="form-group">
				    <label for="password">New Password</label>
				    <input type="password" class="form-control" name="password" placeholder="Password">
				</div>
				<div class="form-group">
				    <label for="password">Re-Enter New Password</label>
				    <input type="password" class="form-control" name="password_2" placeholder="Re-Enter Password">
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
	</div>

@stop