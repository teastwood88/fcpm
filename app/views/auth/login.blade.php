@extends('authMaster')

@section('content')
	
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h1>Login</h1>
			<form role="form" method="POST" action="{{ action('AuthController@login_post') }}">
				<div class="form-group">
					<label for="username">Username</label>
					<input type="text" class="form-control" name="username" placeholder="Enter username">
				</div>
				<div class="form-group">
				    <label for="password">Password</label>
				    <input type="password" class="form-control" name="password" placeholder="Password">
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
	</div>

@stop