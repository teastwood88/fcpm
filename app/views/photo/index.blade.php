@extends('adminMaster')

@section('content')
	<h2>Photos for - {{$property->address}}</h2>

	<ul id="sortable" class="ui-sortable">
		@foreach($photos as $p)
			<li class="ui-sortable-handle" id="{{$p->id}}"><img src="{{asset('/img/'.$p->file_name)}}" width="150"> <a href="{{URL::to('admin/property/photos/delete/'.$p->id)}}" onclick="javascript:return confirm('Are you sure you want to delete this entry?')"><span class="glyphicon glyphicon-remove"></span></a></li>
		@endforeach
	</ul>

	<h3>Add Photos</h3>
	<form action="{{ action('PhotoController@add_post') }}" method="POST" enctype="multipart/form-data">
		<div class="form-group">
	    	<label for="photo">Photo</label>
	    	<input type="file" name="photo">
  		</div>
  		<input type="hidden" name="property_id" value="{{$property->id}}">
  		<button type="submit" class="btn btn-default">Add Photo(s)</button>
	</form>

	<script type="text/javascript">
	$(function() {
		$( "#sortable" ).sortable({
			update: function(event, ui){
				var photo_order = $(this).sortable('toArray');
				//ajax this to a route
				$.post( "{{URL::to('admin/property/photos/sort')}}", { order: photo_order} );
				console.log(photo_order);
			}
		});
	});
	</script>
@stop