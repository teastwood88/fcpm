<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Forest City Property Management - London, Ontario's Best Property Management Company</title>
    <meta name="description" content="Forest City Property Management - London, Ontario's Best Property Management Company">
    <!-- Bootstrap -->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Raleway:600,400,200' rel='stylesheet' type='text/css'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <link href="{{asset('css/pgwslideshow.min.css')}}" rel="stylesheet">

    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/pgwslideshow.min.js')}}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>
  <body>

  	<div class="container-fluid">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
            <div class="contact-info">
            <p>519 719 1603 (24 hour service) <a href="mailto:<?php echo 'info@forestcitypropertymanagement.ca' ?>"><?php echo 'info@forestcitypropertymanagement.ca' ?></a></p>
          </div>
            </div>
        </div>
      <div class="row" id="header">
        <div class="col-md-8 col-md-offset-2">
          <img src="{{URL::to('img/logo.jpg')}}" alt="Forest City Property Management">
          <h1>Forest City <span>Property Management</span></h1>

        </div>
      </div>

      <div class="row" id="nav">
        <div class="col-md-8 col-md-offset-2">
          <ul>
            <li class="nav-first"><a href="{{URL::to('/')}}">HOME</a></li><li>
            <a href="{{URL::to('about-us')}}">ABOUT US</a></li><li>
            <a href="{{URL::to('listings')}}">LISTINGS</a></li><li>
            <a href="{{URL::to('landlords')}}">LANDLORDS</a></li><li class="nav-last">
            <a href="{{URL::to('contact-us')}}">CONTACT US</a></li>
          </ul>
        </div>
      </div>

        @if( Session::has('success') )
            <div class="row" >
                <div class="col-md-8 col-md-offset-2" id="success-flash">
                    <p>{{ Session::get('success') }}</p>
                </div>
            </div>
        @endif


      @yield('content')

      <div class="row" id="footer">
        <div class="col-md-8 col-md-offset-2">
          <ul>
            <li><a href="{{URL::to('/')}}">HOME</a></li>
            <li><a href="{{URL::to('about-us')}}">ABOUT US</a></li>
            <li><a href="{{URL::to('listings')}}">LISTINGS</a></li>
            <li><a href="{{URL::to('landlords')}}">LANDLORDS</a></li>
            <li><a href="{{URL::to('contact-us')}}">CONTACT US</a></li>
          </ul>
          <div class="contact-info">
            <p id="phone_num">519 719 1603<br />(24 hour service)</p>
            <p><a href="mailto:<?php echo 'info@forestcitypropertymanagement.ca' ?>"><?php echo 'info@forestcitypropertymanagement.ca' ?></a></p>
          </div>
        </div>
      </div>

    </div>
  </body>
</html>
