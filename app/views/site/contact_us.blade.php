@extends('siteMaster')

@section('content')
<div class="row" id="contact-us-1">
	<div class="col-md-8 col-md-offset-2">
		<h2 class="text-emphasis">Contact Us</h2>
		<div class="col-md-6 contact">
			<h3>Phone</h3>
			<p>519 719 1603 (24 hour service)</p>
		</div>
		<div class="col-md-6 contact">
			<h3>Email</h3>
			<p><a href="mailto:<?php echo 'info@forestcitypropertymanagement.ca' ?>"><?php echo 'info@forestcitypropertymanagement.ca' ?></a></p>
		</div>
	</div>
</div>

<div class="row bot-mar" id="contact-us-2">
	<div class="col-md-8 col-md-offset-2">
		<h2 class="text-emphasis">Get In Touch!</h2>
		<form class="form-horizontal" role="form" method="POST" action="{{URL::to('contact_us/post')}}">
			<div class="form-group">
				<label for="name" class="col-md-2 control-label">Name</label>
				<div class="col-md-8">
					<input type="text" class="form-control" name="name">
				</div>
			</div>
			<div class="form-group">
				<label for="name" class="col-md-2 control-label">Email</label>
				<div class="col-md-8">
					<input type="email" class="form-control" name="email">
				</div>
			</div>
			<div class="form-group">
				<label for="message" class="col-md-2 control-label">Message</label>
				<div class="col-md-8">
					<textarea class="form-control" rows="5" name="message"></textarea>
				</div>
			</div>
			<div class="col-md-offset-2 col-md-10">
		    	<button type="submit" class="btn btn-default">Submit</button>
		    </div>
		</form>
	</div>
</div>
@stop