@extends('siteMaster')

@section('content')
<div class="row" id="about-us-1">
	<div class="col-md-8 col-md-offset-2">
		<h2 class="text-emphasis">Who We Are</h2>
		<p>Forest City is a rapidly growing property management company focused on the Greater London Area.</p>
		<p>We manage a very diverse portfolio including student rentals, single family homes, and apartment buildings.</p>
	</div>
</div>
<div class="row" id="about-us-2">
	<div class="col-md-6 col-md-offset-3">
		<i class="fa fa-quote-left"></i>
		<p>Our job is to make your day to day life easier, our goal is to help secure your future.</p>
		<i class="fa fa-quote-right pull-right"></i>
	</div>
</div>
<div class="row bot-mar" id="about-us-3">
	<div class="col-md-8 col-md-offset-2">
		<h2 class="text-emphasis">What We Do</h2>
		<p>Simply put, we make life easier for both owners and tenants.</p>
		<p>We cater our management packages to each owner, to eliminate the stress involved with owning and managing property.</p>
		<p>We manage each and every property in an ethical and professional manner, giving you complete peace of mind.</p>
	</div>
</div>
@stop