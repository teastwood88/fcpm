@extends('siteMaster')

@section('content')
<div class="row bot-mar" id="listings-1">
	<div class="col-md-8 col-md-offset-2">
		<h2>Current Listings</h2>

		<div id="property-listing">
			@foreach($properties as $p)
				<div class="row property">
					<div class="col-md-5">
						<a href="{{URL::to('listing/'.str_replace(' ', '_', $p->city).'/'.str_replace(' ', '_', $p->address).'/'.$p->id)}}">
							@if($p->photos != '[]')
								<img src="{{URL::to('img/'.$p->main_photo() )}}" alt="{{ $p->city.', Ontario '.$p->address }}"/>
							@else
								<img src="{{URL::to('img/default.png' )}}" alt="Forest City Property Management"/>
							@endif
						</a>
					</div>
					<div class="col-md-7 property-info">
						<h2><a href="{{URL::to('listing/'.str_replace(' ', '_', $p->city).'/'.str_replace(' ', '_', $p->address).'/'.$p->id)}}" class="text-emphasis">{{$p->address}} @if($p->unit_number != null) Unit {{$p->unit_number}} @endif</a> @if($p->is_rented == 1)<br /><span id="rented">CURRENTLY RENTED</span> @endif</h2>
						<div class="col-md-6">
							<p>{{$p->city}}, Ontario</p>
                            @if($p->cost != 0)
				                <p>${{$p->cost}} / month</p>
                            @else
                                <p>Call for pricing</p>
                            @endif
							<p>{{$p->bathrooms}} bathrooms</p>
							<p>{{$p->bedrooms}} bedrooms</p>
						</div>
						<div class="col-md-6">
							@if(is_array(json_decode($p->extras, true) ) )
							<ul class="styled-list">
								@foreach(array_slice(json_decode($p->extras, true), 0, 5 ) as $e => $x)
									<li><span>{{$e}}</span></li>
								@endforeach
							</ul>
							@endif
						</div>
					</div>
				</div>
			@endforeach
		</div>
		<?php echo $properties->links(); ?>
	</div>
</div>
@stop
