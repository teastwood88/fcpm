@extends('siteMaster')

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<h1>{{$property->address}} @if($property->unit_number != null) Unit {{$property->unit_number}} @endif</h1>

		<div class="col-md-8">
			<ul class="pgwSlideshow">
			    @foreach($property->photos as $photo)
					<li><img src="{{URL::to('img/'.$photo->file_name)}}" alt="{{$property->city.', Ontario '.$property->address.' '.$property->id}}"></li>
				@endforeach
			</ul>
			@if($property->is_rented == 1)
				<h1 id="rented">CURRENTLY RENTED</h1>
			@endif
		</div>
		<div class="col-md-4">
			<h3>Extras</h3>
			<ul class="styled-list">
			@if(is_array(json_decode($property->extras, true) ) )
				@foreach(json_decode($property->extras, true) as $e => $x)
					<li><span>{{$e}}</span></li>
				@endforeach
			@endif
			</ul>
		</div>
	</div>
</div>
<div class="row" id="listing-features">
	<div  class="col-md-8 col-md-offset-2">
		<div class="col-md-12">
            @if($property->description != '')
                <p id="listing-description">{{$property->description}}</p>
            @endif
			<div class="col-md-6">
				@if($property->city != '')
                    <p>{{$property->city}}, Ontario - {{$property->postal_code}}</p>
                @endif
				@if($property->cost != 0)
                    <p>Cost: ${{$property->cost}} / mo</p>
                @else
                    <p>Cost: Call for details</p>
                @endif
			</div>
			<div class="col-md-6">
				@if($property->bedrooms != '')
                    <p>Bedrooms: {{$property->bedrooms}}</p>
                @endif
				@if($property->bathrooms != '')
                    <p>Bathrooms: {{$property->bathrooms}}</p>
                @endif
			</div>		
		</div>
	</div>
</div>



<script type="text/javascript">
$(function(){
    $('.pgwSlideshow').pgwSlideshow();
})
</script>
@stop
