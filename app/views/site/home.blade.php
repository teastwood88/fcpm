@extends('siteMaster')

@section('content')
<div class="row" id="home-1">
	<div class="col-md-8 col-md-offset-2">
		<div class="jumbotron">
		  <h1>Our job is to make your <span class="text-emphasis">day to day life easier,</span> Our goal is to <span class="text-emphasis">help secure your future.</span></h1>
		</div>
	</div>
</div>
<div class="row bot-mar" id="home-3">
	<div class="col-md-8 col-md-offset-2">
		<div class="col-md-6">
			<div class="third-img"><i class="fa fa-users"></i></div>
			<h3 class="text-emphasis">Landlords</h3>
            <p>Let our experienced team take away the stress of ownership, by handling all your day-to-day needs. We offer custom management packages, from locating and vetting tenants, to full service management including rent collection, maintenance requests, and bookkeeping.</p>
			<p><a href="{{URL::to('landlords')}}" class="text-emphasis">Learn more. . .</a></p>
		</div>
		<div class="col-md-6">
			<div class="third-img"><i class="fa fa-home"></i></div>
			<h3 class="text-emphasis">Properties</h3>
            <p>We offer a wide variety of rental properties, including student housing, apartments, townhomes, single family homes, and more. We have advanced knowledge of upcoming vacancies that are not yet listed as well.  If you can't find what you're looking for please feel free to contact us and we would love to help you find that perfect home.</p>
			<p><a href="{{URL::to('listings')}}" class="text-emphasis">Learn more. . .</a></p>
		</div>
	</div>
</div>
<div class="row" id="home-2">
	<div class="col-md-8 col-md-offset-2">
		@foreach($featured as $f)
		<div class="col-md-4 featured-listing">
            <a href="{{URL::to('listing/'.str_replace(' ', '_', $f['city']).'/'.str_replace(' ', '_', $f['address']).'/'.$f['id'])}}">
                <div class="featured-property">
                    <h2>{{$f['address']}} @if($f['unit_number'] != null) Unit {{$f['unit_number']}} @endif</h2>
                    @if(Photo::get_main_photo($f['id']) != '')
                        <img src="{{URL::to('img/'.Photo::get_main_photo($f['id']) )}}" />
                    @else
                        <img src="{{URL::to('img/default.png' )}}" />
                    @endif
                    <ul class="styled-list featured-list">
                        <li><span>Cost: @if( $f['cost'] != 0 ) ${{$f['cost']}} / mo @else Call for Details @endif</span></li>
                        <li><span>Bedrooms: {{$f['bedrooms']}}</span></li>
                        <li><span>Bathrooms: {{$f['bathrooms']}}</span></li>
                    </ul>
                </div>
            </a>
		</div>
		@endforeach
	</div>
</div>
@stop
