@extends('siteMaster')

@section('content')
<div class="row" id="packages-1">
	<div class="col-md-8 col-md-offset-2">
		<h2>Landlords</h2>
		<p>At Forest City Property Management you can rest easy knowing your investment is being well maintained. We can do it all, from finding great tenants, to setting up all mainatance work, to providing detailed monthly financial statements. <span class="text-emphasis">Our goal is to make your investment work for you, so you don't have to work for it.</span></p>
		<p>We are small enough to deliver a personal touch to every property, tenant, and owner, and big enough to provide full management services. We have different packages available depending on your budget and desired level of involvement.</p>

		<h2><span class="text-emphasis">Package A</span></h2>

		<p>Full scale management - no more hassle for you. Let us take care of everything. An initial tenant placement fee of 75% of one month's rent and a 7% monthly fee thereafter, includes the following:</p>
		<ul class="styled-list">
			<li><span>monthly income statements, which are detailed and ready for you and your accountant</span></li>
			<li><span>access to our great repair/maintenance team with preferred rates</span></li>
			<li><span>handling of all maintenance requests</span></li>
			<li><span>finding all tenants, which go through our screening process - credit and employment checks</span></li>
			<li><span>tenant walk throughs</span></li>
			<li><span>we collect the rent and deposit it directly to you, no hassle.</span></li>
			<li><span>can set up grass cutting and snow removal</span></li>
			<li><span>professional photos taken of your property for advertising</span></li>
			<li><span>all advertising is taken care of by Forest City Property Management</span></li>
		</ul>
	</div>
</div>
<div class="row" id="packages-2">
	<div class="col-md-8 col-md-offset-2">
		<h2><span class="text-emphasis">Package B</span></h2>
		<p>75% of first month rent</p>
		<ul class="styled-list">
			<li><span>Let us advertise, show, and fill your property. We will properly vet the tenant, complete the initial walkthrough, the lease agreement, and collect first and last month's rent. We often have current and former tenants contact us directly to find out what we have available - better than taking a chance on an unknown person.</span></li>
		</ul>
	</div>
</div>
<div class="row bot-mar" id="packages-3">
	<div class="col-md-8 col-md-offset-2">
		<h2><span class="text-emphasis">Package C</span></h2>
		<p>This package is tailored to suit you! Every owner is different, and therefore requires a different level of involvement. We can create a custom package to suit your needs. <a href="{{URL::to('contact-us')}}">Contact us today!</a> we would love to sit down and talk about your needs, and how we can best fulfill them.</p>
	</div>
</div>
@stop