<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/admin.css')}}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
  </head>
  <body>

  	<div class="container-fluid">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <a href="{{URL::to('admin/password/reset')}}" class="pull-right">Reset Password</a>
        <h1>Forest City Property Management - Dashboard - <a href="{{URL::to('admin')}}" class="btn btn-primary">HOME</a> - <a href="{{URL::to('admin/contacts')}}" class="btn btn-success">VIEW CONTACTS</a> <a href="{{URL::to('logout')}}" class="btn btn-danger pull-right">LOGOUT</a></h1>
            
        <div class="row">
            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
            @endif
            </div>
        </div>
            
      	@yield('content')
      </div>
    </div>
  </body>
</html>