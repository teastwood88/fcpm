<?php

class HomeController extends BaseController {

	public function home()
	{
		$data['featured'] = Property::where('is_featured', 1)->take(3)->get()->toArray();

		if(count($data['featured']) < 3)
		{
			$old_props = Property::where('is_featured', NULL)->orderBy('created_at', 'ASC')->get()->toArray();

			foreach ($old_props as $prop)
			{
				array_push($data['featured'],  $prop);

				if(count($data['featured'] >= 3))
				{
					break;
				}
			}

		}

		return View::make('site/home', $data);
	}

	public function about_us()
	{
		$data = array();
		return View::make('site/about_us', $data);
	}

	public function listings()
	{
		$data['properties'] = Property::paginate(15);

		return View::make('site/listings', $data);
	}

	public function individual_listing($city, $address, $id)
	{
		$data['property'] = Property::find($id);

		return View::make('site/individual_listing', $data);
	}

	public function landlords()
	{
		$data = array();
		return View::make('site/landlords', $data);
	}

	public function contact_us()
	{
		$data = array();
		return View::make('site/contact_us', $data);
	}

	public function contact_us_post()
	{
		$new = new Contact;

		$new->name = Input::get('name');
		$new->email = Input::get('email');
		$new->message = Input::get('message');

		$new->save();

        Mail::send('emails.contact', array('name' => Input::get('name'), 'email' => Input::get('email'), 'mess' => Input::get('message') ), function($message)
        {
            $message->from('contact@forestcitypropertymanagement.ca', 'Contact Us Form');

            $message->to('info@forestcitypropertymanagement.ca')->subject('Contact Us Form Submission');
        });

		return Redirect::to('contact-us')->with('success', 'Your information has been successfully submitted.  We thank you for your interest.');
	}
}
