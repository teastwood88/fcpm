<?php

class PhotoController extends BaseController {

	public function index($property_id)
	{
		$data['property'] = Property::find($property_id);
		$data['photos'] = Photo::where('property_id', $property_id)->orderBy('photo_order')->get();
		
		return View::make('photo/index', $data);
	}

	public function add_post()
	{
		if(Input::hasFile('photo') )
		{
			$property = Property::find( Input::get('property_id') );
			$photo = Input::file('photo');

			$new = new Photo;
			$filename = str_replace(' ', '_', $property->address).'_'.time().'.'.$photo->getClientOriginalExtension();
			
			$new->file_name = $filename;
			$new->property_id = Input::get('property_id');
			$order = count($property->photos);
			$new->photo_order = $order;

			if( $new->save() )
			{
				$photo->move( public_path().'/img',$filename);

				return Redirect::to('admin/property/photos/'.Input::get('property_id') );
			}
		}
	}

	public function sort_photo()
	{
		$new_order = 0;
		foreach(Input::get('order') as $order)
		{
			$photo = Photo::find($order);
			$photo->photo_order = $new_order;
			$photo->save();
			++$new_order;
		}
	}

	public function delete($id)
	{
		$photo = Photo::find($id);
		$file_name = $photo->file_name;

		if($photo->delete() )
		{
			unlink(public_path()."/img/$file_name");
			return Redirect::to('admin/property/photos/'.$photo->property_id);
		}
	}
}