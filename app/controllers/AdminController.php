<?php

class AdminController extends BaseController {

	public function home()
	{
		$data['properties'] = Property::all();

		return View::make('admin/home', $data);
	}

	public function add_property()
	{
		$data['extras'] = array('Basement', 'Pet Friendly', 'Laundry', 'Parking', 'Fridge', 'Dishwasher', 'Proximity to Schools', 'Proximity to Public Transportation', 'Proximity to Shopping', 'Outdoor Space', 'Heating', 'Air Conditioning');
		return View::make('admin/propertyForm', $data);
	}

	public function add_property_post()
	{
		$new = new Property;

		$new->city = Input::get('city');
		$new->address = Input::get('address');
		$new->unit_number = Input::get('unit_number');
		$new->postal_code = Input::get('postal_code');
		$new->cost = Input::get('cost');
		$new->bedrooms = Input::get('bedrooms');
		$new->bathrooms = Input::get('bathrooms');
		$new->is_rented = Input::get('is_rented');
		$new->is_featured = Input::get('is_featured');
		$new->extras = json_encode(Input::get('extras') );
		$new->description = Input::get('description');

		if($new->save() )
		{
			return Redirect::to('admin');
		}

		return Redirect::to('admin/property/add');
	}

	public function edit_property($id)
	{
		$data['extras'] = array('Basement', 'Pet Friendly', 'Laundry', 'Parking', 'Fridge', 'Dishwasher', 'Proximity to Schools', 'Proximity to Public Transportation', 'Proximity to Shopping', 'Outdoor Space', 'Heating', 'Air Conditioning');
		$data['property'] = Property::find($id);

		return View::make('admin/propertyForm', $data);
	}

	public function edit_property_post()
	{
		$edit = Property::find( Input::get('property_id') );

		$edit->city = Input::get('city');
		$edit->address = Input::get('address');
		$edit->unit_number = Input::get('unit_number');
		$edit->postal_code = Input::get('postal_code');
		$edit->cost = Input::get('cost');
		$edit->bedrooms = Input::get('bedrooms');
		$edit->bathrooms = Input::get('bathrooms');
		$edit->is_rented = Input::get('is_rented');
		$edit->is_featured = Input::get('is_featured');
		$edit->extras = json_encode(Input::get('extras') );
		$edit->description = Input::get('description');

		if($edit->save() )
		{
			return Redirect::to('admin');
		}

		return Redirect::to('admin/property/edit/'.Input::get('property_id') );
	}

	public function delete_property($id)
	{
		$delete = Property::find($id);

		if($delete->delete() )
		{
			//delete all the photos as well
			foreach($delete->photos as $p)
			{
				if($p->delete() )
				{
					unlink(public_path()."/img/".$p->file_name);
				}
			}
			return Redirect::to('admin');
		}

		return Redirect::to('admin');
	}

	public function view_contacts()
	{
		$data['contacts'] = Contact::all();

		return View::make('admin/contacts', $data);
	}

}
