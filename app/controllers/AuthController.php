<?php

class AuthController extends BaseController {

	public function login()
	{
		return View::make('auth/login');
	}

	public function login_post()
	{
		if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password'))))
		{
		    return Redirect::intended('admin');
		}

		return Redirect::to('login')->with('error', 'Your username/password were incorrect');
	}

	public function logout()
	{
		Auth::logout();

		return Redirect::to('/');
	}
    
    public function password_reset()
    {
        return View::make('auth/password_reset');
    }
    
    public function password_reset_post()
    {        
        if(Input::get('password') === Input::get('password_2'))
        {
            Auth::user()->password = Hash::make(Input::get('password'));
            
            if(Auth::user()->save() )
            {
                return Redirect::back()->with('success', 'You have successfully changed your password');
            }
        }
        
        return Redirect::back()->with('error', 'There was an error when trying to change your password');
    }
}
