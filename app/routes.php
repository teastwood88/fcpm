<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//site routes
Route::get('/', 'HomeController@home');
Route::get('about-us', 'HomeController@about_us');
Route::get('listings', 'HomeController@listings');
Route::get('landlords', 'HomeController@landlords');
Route::get('contact-us', 'HomeController@contact_us');
Route::get('listing/{city}/{address}/{id}', 'HomeController@individual_listing');

Route::post('contact_us/post', 'HomeController@contact_us_post');


//admin routes
Route::get('admin', array('before' => 'auth', 'uses' => 'AdminController@home') );
Route::get('admin/contacts', array('before' => 'auth', 'uses' => 'AdminController@view_contacts') );

Route::get('admin/property/add', array('before' => 'auth', 'uses' => 'AdminController@add_property') );
Route::post('admin/property/add/post', array('before' => 'auth', 'uses' => 'AdminController@add_property_post') );

Route::get('admin/property/edit/{id}', array('before' => 'auth', 'uses' => 'AdminController@edit_property') );
Route::post('admin/property/edit/post', array('before' => 'auth', 'uses' => 'AdminController@edit_property_post') );

Route::get('admin/property/delete/{id}', array('before' => 'auth', 'uses' => 'AdminController@delete_property') );

//photo routes
Route::get('admin/property/photos/{id}', array('before' => 'auth', 'uses' => 'PhotoController@index') );
Route::post('admin/property/photos/add_post', array('before' => 'auth', 'uses' => 'PhotoController@add_post') );
Route::post('admin/property/photos/sort', array('before' => 'auth', 'uses' => 'PhotoController@sort_photo') );
Route::get('admin/property/photos/delete/{id}', array('before' => 'auth', 'uses' => 'PhotoController@delete') );


//auth routes
Route::get('login', 'AuthController@login');
Route::post('login_post', 'AuthController@login_post');

Route::get('admin/password/reset', array('before' => 'auth', 'uses' => 'AuthController@password_reset') );
Route::post('admin/password/reset/post', array('before' => 'auth', 'uses' => 'AuthController@password_reset_post') );

Route::get('logout', 'AuthController@logout');
