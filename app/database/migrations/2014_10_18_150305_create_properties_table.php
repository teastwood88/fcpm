<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('properties', function($table)
		{
			$table->increments('id');
		    $table->string('address')->nullable();
		    $table->string('unit_number')->nullable();
		    $table->string('postal_code')->nullable();
		    $table->string('city')->nullable();
		    $table->integer('bedrooms')->nullable();
		    $table->integer('bathrooms')->nullable();
		    $table->integer('cost')->nullable();
		    $table->text('description')->nullable();
		    $table->integer('is_rented')->nullable();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('properties');
	}

}
