<?php

class Property extends Eloquent {

	protected $table = 'properties';

	public function photos()
	{
		return $this->hasMany('Photo', 'property_id');
	}

	public function main_photo()
	{
                if(is_object($this->photos()->where('photo_order', 0)->first() ) ){
		     return $this->photos()->where('photo_order', 0)->first()->toArray()['file_name'];
                }
                return '';
	}

}