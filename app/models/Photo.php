<?php

class Photo extends Eloquent {

	protected $table = 'photos';

	public function property()
	{
		return $this->belongsTo('Property', 'property_id');
	}

	public static function get_main_photo($id)
	{
		$main_photo = Photo::where('photo_order', 0)->where('property_id', $id)->first()->file_name;

		return $main_photo;
	}
	
}